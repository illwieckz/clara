/*-
 * Copyright 2009, 2010 Bjoern Koenig
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>

#include "clara.h"
#include "lock.h"

bool
lock_create(lock_t *result)
{
	lock_t lock;

	if ((lock = malloc(sizeof(*lock))) == NULL)
	{
		errno = ENOMEM;
		return false;
	}

	if ((lock->sem = malloc(sizeof(*lock->sem))) == NULL)
	{
		free(lock);
		errno = ENOMEM;
		return false;
	}

	if (sem_init(lock->sem, 0, 1) == -1)
	{
		debug_printff("sem_init failed, errno = %d (%s)", errno, strerror(errno));
		free(lock->sem);
		free(lock);
		return false;
	}

	lock->owner = 0;
	lock->cnt = 0;

	*result = lock;
//	debug_printff("(%lx)", lock);
	return true;
}

bool
lock_destroy(lock_t lock)
{
	sem_t *sem;
//	debug_printff("(%lx)", lock);

	if (lock == NULL)
	{
		errno = EINVAL;
		return false;
	}

	lock_wait(lock);
	sem = lock->sem;
	if (lock->cnt != 1)
	{
		debug_printff("(%lx) you destroy a locked lock"
			" (cnt=%u, owner=%lx, self=%lx)",
			lock, lock->cnt, lock->owner, pthread_self());
	}

	free(lock);
	if (sem_destroy(sem) == -1)
		debug_printff("sem_destroy failed, errno = %d (%s)", errno, strerror(errno));
	free(sem);

	return true;
}

void
lock_wait(lock_t lock)
{
	pthread_t tid = pthread_self();
	if (tid == 0)
	{
		debug_printff("thread with ID 0 can't wait for a lock");
		return;
	}

	if ((lock->owner == 0) || (lock->owner != tid))
	{
		if (sem_wait(lock->sem) == -1)
		{
			debug_printff("sem_wait failed, errno = %d (%s)", errno, strerror(errno));
		}
		else if (lock->owner == 0)
			lock->owner = tid;
	}
	lock->cnt++;
//	debug_printff("(%lx) tid=%lx cnt=%u", lock, tid, lock->cnt);
}

void
lock_post(lock_t lock)
{
	pthread_t tid = pthread_self();
//	debug_printff("(%lx) tid=%lx cnt=%u", lock, tid, lock->cnt);
	if (tid == 0)
	{
		debug_printff("thread with ID 0 can't post a lock");
		return;
	}
	if (lock->owner != tid)
	{
		debug_printff("owner=%lx != self=%lx", lock->owner, tid);
		return;
	}

	if (--lock->cnt == 0)
	{ 
		lock->owner = 0;
		if (sem_post(lock->sem) == -1)
		{
			debug_printff("sem_post failed, errno = %d (%s)", errno, strerror(errno));
		}
	}
}


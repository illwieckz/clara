#include "object.h"

bool
object_create(object_t *result, provider_t provider, void *real_id, global_id_t id, object_type_t type)
{
	object_t object;

	if ((real_id == NULL) || (provider == NULL) || (id == INVALID_ID))
	{
		errno = EINVAL;
		return false;
	}

	if ((object = calloc(1, sizeof(*object))) == NULL)
	{
		errno = ENOMEM;
		return false;
	}

	if (!lock_create(&object->lock))
	{
		debug_printff("lock_create failed");
		free(object);
		return false;
	}

	object->provider = provider;

	object->real_id = real_id;
	object->id = id;
	object->type = type;

	agent_t agent = provider->agent;
	if (!set_insert(agent->objects, id, object))
	{
		debug_printff("set_insert failed" TSNH);
		if (!lock_destroy(object->lock))
		{
			debug_printff("lock_destroy failed" TSNH);
		}
		free(object);
		return false;
	}

	if (result != NULL)
		*result = object;

	return true;
}

bool
object_retain(object_t object, consumer_t consumer)
{
	lock_wait(object->lock);
	if ((object->refcnt > 0) && (consumer != object->consumer))
	{
		debug_printff("access violation");
		lock_post(object->lock);
		return false;
	}
	else if (object->refcnt == 0)
	{
		object->consumer = consumer;
		if (!set_insert(consumer->objects, object->id, object))
		{
			debug_printff("set_insert failed" TSNH);
			lock_post(object->lock);
			return false;
		}
	}
	object->refcnt++;
	lock_post(object->lock);

	return true;
}

bool
object_release(object_t object)
{
	lock_wait(object->lock);
	object->refcnt--;

	if (object->refcnt == 0)
	{
		if (!set_remove(object->consumer->objects, object->id))
		{
			debug_printff("set_remove failed" TSNH);
		}
		object->consumer = NULL;
	}

	lock_post(object->lock);

	return true;
}

bool
object_destroy(object_t object)
{
//	for now assume that the object is locked
//	lock_wait(object->lock);
	if (object->refcnt != 0)
	{
		debug_printff("(%lx) refcnt = %u", object, object->refcnt);
		return false;
	}

	agent_t agent = object->provider->agent;
	if (!set_remove(agent->objects, object->id))
	{
		debug_printff("set_remove failed" TSNH);
	}

	lock_t lock;
	lock = object->lock;
	free(object);
	lock_post(lock);
	lock_destroy(lock);
	return true;
}


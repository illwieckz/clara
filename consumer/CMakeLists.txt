cmake_minimum_required(VERSION 3.1)

if(${CLARA_PROJECT_NAME})
	set(LIBCLARA_BINARY_DIR ${CLARA_BINARY_DIR})
else()
	set(LIBCLARA_PROJECT_NAME libclara)
	project(${LIBCLARA_PROJECT_NAME})
	set(LIBCLARA_BINARY_DIR ${CMAKE_BINARY_DIR})
endif()

set(LIBCLARA_LIBRARY_NAME clara)

set(CL_TARGET_OPENCL_VERSION 110 CACHE STRING "OpenCL version to target")
add_compile_definitions(CL_TARGET_OPENCL_VERSION=${CL_TARGET_OPENCL_VERSION})

include_directories(../include)

set(LIBCLARA_SOURCE_FILES
	src/consumer.c
	src/opencl_cmdqueue.c
	src/opencl_context.c
	src/opencl_device.c
	src/opencl_event.c
	src/opencl_image.c
	src/opencl_kernel.c
	src/opencl_memobj.c
	src/opencl_platform.c
	src/opencl_program.c
	src/opencl_sampler.c

	src/consumer.h

	../common/api.c
	../common/aux.c
	../common/block.c
	../common/net.c
	../common/rpc_client.c
	../common/rpc_server.c
	../common/stream.c

	../include/api.h
	../include/clara.h
	../include/message.h
	../include/debug.h
	../include/ndebug.h
)

find_package(PkgConfig REQUIRED)
pkg_check_modules(OPENCL REQUIRED IMPORTED_TARGET OpenCL)
pkg_check_modules(SCTP REQUIRED IMPORTED_TARGET libsctp)

set(LIBCLARA_OBJECT_NAME ${LIBCLARA_LIBRARY_NAME}-obj)
add_library(${LIBCLARA_OBJECT_NAME} OBJECT ${LIBCLARA_SOURCE_FILES})
set_property(TARGET ${LIBCLARA_OBJECT_NAME} PROPERTY POSITION_INDEPENDENT_CODE 1)

if(BUILD_SHARED_LIBS)
	set(LIBCLARA_SHARED_LIBRARY_NAME ${LIBCLARA_LIBRARY_NAME})
	set(LIBCLARA_STATIC_LIBRARY_NAME ${LIBCLARA_LIBRARY_NAME}-static)
else()
	set(LIBCLARA_STATIC_LIBRARY_NAME ${LIBCLARA_LIBRARY_NAME})
	set(LIBCLARA_SHARED_LIBRARY_NAME ${LIBCLARA_LIBRARY_NAME}-shared)
endif()

add_library(${LIBCLARA_SHARED_LIBRARY_NAME} SHARED $<TARGET_OBJECTS:${LIBCLARA_OBJECT_NAME}>)
add_library(${LIBCLARA_STATIC_LIBRARY_NAME} STATIC $<TARGET_OBJECTS:${LIBCLARA_OBJECT_NAME}>)

target_link_libraries(${LIBCLARA_STATIC_LIBRARY_NAME}
	PkgConfig::OPENCL
	PkgConfig::SCTP
)

target_link_libraries(${LIBCLARA_SHARED_LIBRARY_NAME}
	PkgConfig::OPENCL
	PkgConfig::SCTP
)

if(NOT WIN32)
	set_target_properties(${LIBCLARA_SHARED_LIBRARY_NAME} PROPERTIES OUTPUT_NAME ${LIBCLARA_LIBRARY_NAME})
	set_target_properties(${LIBCLARA_STATIC_LIBRARY_NAME} PROPERTIES OUTPUT_NAME ${LIBCLARA_LIBRARY_NAME})
endif()

set_target_properties(${LIBCLARA_SHARED_LIBRARY_NAME} PROPERTIES
	LIBRARY_OUTPUT_DIRECTORY ${LIBCLARA_BINARY_DIR})
set_target_properties(${LIBCLARA_STATIC_LIBRARY_NAME} PROPERTIES
	ARCHIVE_OUTPUT_DIRECTORY ${LIBCLARA_BINARY_DIR})

target_link_libraries(${LIBCLARA_SHARED_LIBRARY_NAME})
target_link_libraries(${LIBCLARA_STATIC_LIBRARY_NAME})

install(TARGETS ${LIBCLARA_SHARED_LIBRARY_NAME} DESTINATION lib)
install(TARGETS ${LIBCLARA_STATIC_LIBRARY_NAME} DESTINATION lib)

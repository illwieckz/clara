#include <stdio.h>
#include <CL/cl.h>

#define MAX_PLATFORMS	50

int
main()
{
	cl_int retval;
	cl_platform_id platforms[MAX_PLATFORMS];
	cl_uint num_platforms;

	retval = clGetPlatformIDs(MAX_PLATFORMS, platforms, &num_platforms);

	if (retval != CL_SUCCESS)
	{
		fprintf(stderr, "clGetPlatformIDs failed\n");
		return (-1);
	}

	fprintf(stderr, "clGetPlatformIDs returned %u platforms\n", num_platforms);

	return (0);
}


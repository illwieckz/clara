#include <stdio.h>
#include <CL/cl.h>

char *kernel[] = {
		"__kernel void myKernel(__global unsigned int *out, __global unsigned int *in, const unsigned int x) {",
		"  uint tid = get_global_id(0);",
		"  out[tid] = in[tid] * x; }"
	};

void
show_device_information(cl_platform_id platform_id, cl_device_type device_type)
{
	int i;
	cl_int status;
	cl_device_id devices[100];
	cl_uint num_devices;
	status = clGetDeviceIDs(platform_id, device_type, 100, devices, &num_devices);
	if (status == CL_DEVICE_NOT_FOUND)
	{
		switch (device_type)
		{
		case CL_DEVICE_TYPE_CPU:
			fprintf(stderr, "    no CPU devices found\n");
			break;
		case CL_DEVICE_TYPE_GPU:
			fprintf(stderr, "    no GPU devices found\n");
			break;
		case CL_DEVICE_TYPE_ACCELERATOR:
			fprintf(stderr, "    no ACCELERATOR devices found\n");
			break;
		case CL_DEVICE_TYPE_DEFAULT:
			fprintf(stderr, "    no DEFAULT device found\n");
			break;
		}
	}
	else if (status == CL_SUCCESS)
	{
		fprintf(stderr, "\n    Number of devices: %u\n", num_devices);
		fprintf(stderr, "    -------- devices --------\n");
		for (i = 0; i < num_devices; i++)
		{
			fprintf(stderr, "    Device ID #%i: %08lx\n", i, (long)devices[i]);
			if (device_type != CL_DEVICE_TYPE_DEFAULT)
			{
				size_t sz;

				cl_uint device_vendor_id;
				cl_uint device_max_compute_units;
				cl_uint device_max_work_item_dimensions;
				size_t device_max_work_item_sizes[10];
				size_t device_max_work_group_size;
				cl_uint device_preferred_vector_width_char;
				cl_uint device_preferred_vector_width_short;
				cl_uint device_preferred_vector_width_int;
				cl_uint device_preferred_vector_width_long;
				cl_uint device_preferred_vector_width_float;
				cl_uint device_preferred_vector_width_double;
				cl_uint device_max_clock_frequency;
				cl_uint device_address_bits;
				cl_ulong device_max_mem_alloc_size;
				cl_bool device_image_support;
				cl_uint device_max_read_image_args;
				cl_uint device_max_write_image_args;
				size_t device_image2d_max_width;
				size_t device_image2d_max_height;
				size_t device_image3d_max_width;
				size_t device_image3d_max_height;
				size_t device_image3d_max_depth;
				cl_uint device_max_samplers;
				size_t device_max_parameter_size;
				cl_uint device_mem_base_addr_align;
				cl_uint device_min_data_type_align_size;
				cl_device_fp_config device_single_fp_config;
				cl_device_mem_cache_type device_global_mem_cache_type;
				cl_uint device_global_mem_cacheline_size;
				cl_ulong device_global_mem_cache_size;
				cl_ulong device_global_mem_size;
				cl_ulong device_max_constant_buffer_size;
				cl_uint device_max_constant_args;
				cl_device_local_mem_type device_local_mem_type;
				cl_ulong device_local_mem_size;
				cl_bool device_error_correction_support;
				size_t device_profiling_timer_resolution;
				cl_bool device_endian_little;
				cl_bool device_available;
				cl_bool device_compiler_available;
				cl_device_exec_capabilities device_execution_capabilities;
				cl_command_queue_properties device_queue_properties;
				char device_name[100];
				char device_vendor[100];
				char driver_version[100];
				char device_profile[100];
				char device_version[100];
				char device_extensions[1000];

				status = clGetDeviceInfo(devices[i], CL_DEVICE_NAME, sizeof(device_name), &device_name, &sz);
				if (status == CL_SUCCESS) fprintf(stderr, "        Name                     : %s\n", device_name);
				status = clGetDeviceInfo(devices[i], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(device_max_compute_units), &device_max_compute_units, &sz);
				if (status == CL_SUCCESS) fprintf(stderr, "        Compute units            : %u\n", device_max_compute_units);

				status = clGetDeviceInfo(devices[i], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(device_max_clock_frequency), &device_max_clock_frequency, &sz);
				if (status == CL_SUCCESS) fprintf(stderr, "        Clock frequency          : %u MHz\n", (unsigned int)device_max_clock_frequency);
//				status = clGetDeviceInfo(devices[i], CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(device_max_mem_alloc_size), &device_max_mem_alloc_size, &sz);
//			if (status == CL_SUCCESS) fprintf(stderr, "        MAX_MEM_ALLOC_SIZE       : %u MiB (%u)\n", (unsigned int)(device_max_mem_alloc_size >> 20), (unsigned int)(device_max_mem_alloc_size));
//				status = clGetDeviceInfo(devices[i], CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(device_global_mem_cache_size), &device_global_mem_cache_size, &sz);
//				if (status == CL_SUCCESS) fprintf(stderr, "        GLOBAL_MEM_CACHE_SIZE     : %lu B\n", (unsigned long)device_global_mem_cache_size);
//				status = clGetDeviceInfo(devices[i], CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(device_global_mem_size), &device_global_mem_size, &sz);
//			if (status == CL_SUCCESS) fprintf(stderr, "        GLOBAL_MEM_SIZE           : %lu MiB (%lu)\n", (unsigned long)(device_global_mem_size >> 20), (unsigned long)device_global_mem_size);
//				status = clGetDeviceInfo(devices[i], CL_DEVICE_LOCAL_MEM_SIZE, sizeof(device_local_mem_size), &device_local_mem_size, &sz);
//				if (status == CL_SUCCESS) fprintf(stderr, "        LOCAL_MEM_SIZE           : %lu kiB (%lu)\n", (unsigned long)(device_local_mem_size >> 10), (unsigned long)device_local_mem_size);
//				status = clGetDeviceInfo(devices[i], CL_DEVICE_AVAILABLE, sizeof(device_available), &device_available, &sz);
//				if (status == CL_SUCCESS) fprintf(stderr, "        AVAILABLE                : %s\n", device_available ? "yes" : "no");
//				status = clGetDeviceInfo(devices[i], CL_DEVICE_COMPILER_AVAILABLE, sizeof(device_compiler_available), &device_compiler_available, &sz);
//				if (status == CL_SUCCESS) fprintf(stderr, "        COMPILER_AVAILABLE       : %s\n", device_compiler_available ? "yes" : "no");
//				status = clGetDeviceInfo(devices[i], CL_DEVICE_EXTENSIONS, sizeof(device_extensions), &device_extensions, &sz);
//				if (status == CL_SUCCESS) fprintf(stderr, "        DEVICE_EXTENSIONS : %s\n", device_extensions);

//				status = clGetDeviceInfo(devices[i], CL_DEVICE_, sizeof(device_), &device_, &sz);
//				if (status == CL_SUCCESS) fprintf(stderr, "         : %u\n", device_);
			}
		}
		fprintf(stderr, "\n");
	}
	fprintf(stderr, "\n");
}

int
main()
{
	cl_int status;
	int i;

	/* platform-specific information */
	cl_platform_id platforms[10];
	cl_uint num_platforms;
	status = clGetPlatformIDs(10, platforms, &num_platforms);
	if (status != CL_SUCCESS)
		fprintf(stderr, "clGetPlatformIDs failed\n");
	else
	{
		fprintf(stderr, "Total number of platforms: %u\n", num_platforms);
#if 1
		for (i = 0; i < num_platforms; i++)
		{
			size_t sz;
			char platform_profile[100], platform_version[100], platform_name[100], platform_vendor[100], platform_extensions[1000];
			fprintf(stderr, "================ Platform ID #%u: %08lx ================\n", i, (unsigned long)platforms[i]);
			status = clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, sizeof(platform_name), platform_name, &sz);
			if (status == CL_SUCCESS) fprintf(stderr, "    Name    : %s\n", platform_name);
			status = clGetPlatformInfo(platforms[i], CL_PLATFORM_VERSION, sizeof(platform_version), platform_version, &sz);
			if (status == CL_SUCCESS) fprintf(stderr, "    Version : %s\n", platform_version);
			
			/* device-specific information */
			show_device_information(platforms[i], CL_DEVICE_TYPE_GPU);
		}
#endif
	}

	return (0);
}

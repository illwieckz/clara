#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>

cl_int data[] = { 1, 2, 3, 5, 8, 13, 21, 34 };

const char *sourcecode[] =
{
	"__kernel void mul(__global int *buf)\n",
	"{\n",
	" uint tid = get_global_id(0);\n",
	" int multiplier = 3;\n",
	" buf[tid] *= multiplier;\n",
	"}\n",
	"\n",
	"__kernel void add(__global int *buf, int addend)\n",
	"{\n",
	" uint tid = get_global_id(0);\n",
	" buf[tid] += addend;\n",
	"}\n"
};

void
print_data(size_t n)
{
	int i;
	for (i = 0; i < n; i++)
	printf("%u\n", data[i]);
}



int
main()
{
	cl_uint loc = sizeof(sourcecode) / sizeof(*sourcecode); /* Anzahl der Quelltextzeilen */
	cl_ulong n = sizeof(data) / sizeof(*data);		/* Anzahl der Datenelemente */
	cl_int x = 36;						/* Parameter für add-Kernel */

	/* Zielplattform ermitteln */
	cl_platform_id platform[1];
	clGetPlatformIDs(1, platform, NULL);

	/* Zielgerät ermitteln */
	cl_device_id device[1];
	clGetDeviceIDs(platform[0], CL_DEVICE_TYPE_GPU, 1, device, NULL);

	/* device available? */
	cl_bool available;
	clGetDeviceInfo(device[0], CL_DEVICE_AVAILABLE, sizeof(available), &available, NULL);
	if (!available)
	{
		printf("device is not available\n");
		return 0;
	}

	/* Kontext definieren */
	cl_context ctx;
	cl_context_properties *ctx_prop;
	ctx_prop = (cl_context_properties *)calloc(3, sizeof(*ctx_prop));
	ctx_prop[0] = CL_CONTEXT_PLATFORM;
	ctx_prop[1] = (cl_context_properties)platform[0];

	ctx = clCreateContext(ctx_prop, 1, device, NULL, NULL, NULL);

	/* Befehlswarteschlange erstellen */
	cl_command_queue cq;
	cq = clCreateCommandQueue(ctx, device[0], 0, NULL);

	/* Speicherobjekt erstellen */
	cl_mem mem;
	mem = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(data), NULL, NULL);

	/* Programm kompilieren */
	cl_program program;
	program = clCreateProgramWithSource(ctx, loc, sourcecode, NULL, NULL);
	clBuildProgram(program, 1, device, NULL, NULL, NULL);
	
	/* Kernelobjekte erstellen */
	cl_kernel kernelM, kernelA;
	kernelM = clCreateKernel(program, "mul", NULL);
	kernelA = clCreateKernel(program, "add", NULL);

	/* Kernelparameter für mul-Kernel festlegen */
	clSetKernelArg(kernelM, 0, sizeof(cl_mem), (void *)&mem);
	/* Kernelparameter für add-Kernel festlegen */
	clSetKernelArg(kernelA, 0, sizeof(cl_mem), (void *)&mem);
	clSetKernelArg(kernelA, 1, sizeof(cl_int), (void *)&x);

	/* Befehlswarteschlange füllen:
	 * * - Daten in den Speicher kopieren
	 * * - mul-Kernel anwenden
	 * * - add-Kernel anwenden
	 * * - Daten aus dem Speicher lesen
	 * *
	 * und blockieren bis der letzte Befehl abgearbeitet ist */
	clEnqueueWriteBuffer(cq, mem, CL_FALSE, 0, sizeof(data), data, 0, NULL, NULL);
	clEnqueueNDRangeKernel(cq, kernelM, 1, NULL, &n, NULL, 0, NULL, NULL);
	clEnqueueNDRangeKernel(cq, kernelA, 1, NULL, &n, NULL, 0, NULL, NULL);
	clEnqueueReadBuffer(cq, mem, CL_TRUE, 0, sizeof(data), data, 0, NULL, NULL);

	/* Ausgabe (39, 42, 45, 51, 60, 75, 99, 138) */
	print_data(n);

	/* Aufräumen */
	clReleaseKernel(kernelM);
	clReleaseKernel(kernelA);
	clReleaseProgram(program);
	clReleaseMemObject(mem);
	clReleaseCommandQueue(cq);
	clReleaseContext(ctx);

	return (0);
}


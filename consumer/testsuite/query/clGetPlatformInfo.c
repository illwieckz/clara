/*-
 * Copyright 2009, 2010 Bjoern Koenig
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>

struct platform_info_t {
	cl_platform_info value;
	const char *name;
};

struct platform_info_t platform_info_list[] = {
	{ CL_PLATFORM_PROFILE, "PROFILE" },
	{ CL_PLATFORM_VERSION, "VERSION" },
	{ CL_PLATFORM_NAME, "NAME" },
	{ CL_PLATFORM_VENDOR, "VENDOR" },
	{ CL_PLATFORM_EXTENSIONS, "EXTENSIONS" },
	{ 0, NULL }
};

void
usage(void)
{
	fprintf(stderr, "usage: clGetPlatformInfo [platform_id]\n");
	exit(1);
}

int
main(int argc, char *argv[])
{
	cl_int status;

	if (argc != 2)
		usage();

	cl_platform_id platform = (cl_platform_id)strtol(argv[1], NULL, 16);

	void *param_value;
	size_t param_value_size;
	struct platform_info_t *platform_info;
	for (platform_info = platform_info_list; platform_info->name != NULL; platform_info++)
	{
		status = clGetPlatformInfo(platform, platform_info->value, 0, NULL, &param_value_size);
		if (status != CL_SUCCESS)
		{
			printf("clGetPlatformInfo failed, platform_info = %s, status = %d\n", platform_info->name, status);
			continue;
		}
		else
		{
			param_value = malloc(param_value_size);
			assert(param_value != NULL);

			status = clGetPlatformInfo(platform, platform_info->value, param_value_size, param_value, NULL);
			if (status != CL_SUCCESS)
			{
				printf("clGetPlatformInfo failed, platform_info = %s, status = %d\n", platform_info->name, status);
				free(param_value);
				continue;
			}
			printf("%20s = \"%s\"\n", platform_info->name, (char *)param_value);
			free(param_value);
		}
	}

	return 0;
}


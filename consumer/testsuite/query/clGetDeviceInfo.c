/*-
 * Copyright 2009, 2010 Bjoern Koenig
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>

enum device_info_type_t {
	STRING,
	UINT,
	ULONG,
	SIZE,
	BOOL,
	BITFIELD
};

struct device_info_t {
	cl_device_info value;
	enum device_info_type_t type;
	const char *name;
};

struct device_info_t device_info_list[] = {
	{ CL_DEVICE_TYPE, BITFIELD, "TYPE" },
	{ CL_DEVICE_VENDOR, STRING, "VENDOR" },
	{ CL_DEVICE_MAX_COMPUTE_UNITS, UINT, "MAX_COMPUTE_UNITS" },
	{ CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, UINT, "MAX_WORK_ITEM_DIMENSIONS" },
	{ CL_DEVICE_MAX_WORK_GROUP_SIZE, SIZE, "MAX_WORK_GROUP_SIZE" },
	{ CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, UINT, "PREFERRED_VECTOR_WIDTH_CHAR" },
	{ CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, UINT, "PREFERRED_VECTOR_WIDTH_SHORT" },
	{ CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, UINT, "PREFERRED_VECTOR_WIDTH_INT" },
	{ CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, UINT, "PREFERRED_VECTOR_WIDTH_LONG" },
	{ CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, UINT, "PREFERRED_VECTOR_WIDTH_FLOAT" },
	{ CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, UINT, "PREFERRED_VECTOR_WIDTH_DOUBLE" },
	{ CL_DEVICE_MAX_CLOCK_FREQUENCY, UINT, "MAX_CLOCK_FREQUENCY" },
	{ CL_DEVICE_ADDRESS_BITS, UINT, "ADDRESS_BITS" },
	{ CL_DEVICE_MAX_MEM_ALLOC_SIZE, ULONG, "MAX_MEM_ALLOC_SIZE" },
	{ CL_DEVICE_IMAGE_SUPPORT, BOOL, "IMAGE_SUPPORT" },
	{ CL_DEVICE_MAX_READ_IMAGE_ARGS, UINT, "MAX_READ_IMAGE_ARGS" },
	{ CL_DEVICE_MAX_WRITE_IMAGE_ARGS, UINT, "MAX_WRITE_IMAGE_ARGS" },
	{ CL_DEVICE_IMAGE2D_MAX_WIDTH, SIZE, "IMAGE2D_MAX_WIDTH" },
	{ CL_DEVICE_IMAGE2D_MAX_HEIGHT, SIZE, "IMAGE2D_MAX_HEIGHT" },
	{ CL_DEVICE_IMAGE3D_MAX_WIDTH, SIZE, "IMAGE3D_MAX_WIDTH" },
	{ CL_DEVICE_IMAGE3D_MAX_HEIGHT, SIZE, "IMAGE3D_MAX_HEIGHT" },
	{ CL_DEVICE_IMAGE3D_MAX_DEPTH, SIZE, "IMAGE3D_MAX_DEPTH" },
	{ CL_DEVICE_MAX_SAMPLERS, UINT, "MAX_SAMPLERS" },
	{ CL_DEVICE_MAX_PARAMETER_SIZE, SIZE, "MAX_PARAMETER_SIZE" },
	{ CL_DEVICE_MEM_BASE_ADDR_ALIGN, UINT, "MEM_BASE_ADDR_ALIGN" },
	{ CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE, UINT, "MIN_DATA_TYPE_ALIGN_SIZE" },
	{ CL_DEVICE_SINGLE_FP_CONFIG, BITFIELD, "SINGLE_FP_CONFIG" },
	{ CL_DEVICE_GLOBAL_MEM_CACHE_TYPE, BITFIELD, "GLOBAL_MEM_CACHE_TYPE" },
	{ CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, UINT, "GLOBAL_MEM_CACHELINE_SIZE" },
	{ CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, ULONG, "GLOBAL_MEM_CACHE_SIZE" },
	{ CL_DEVICE_GLOBAL_MEM_SIZE, ULONG, "GLOBAL_MEM_SIZE" },
	{ CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, ULONG, "MAX_CONSTANT_BUFFER_SIZE" },
	{ CL_DEVICE_MAX_CONSTANT_ARGS, UINT, "MAX_CONSTANT_ARGS" },
	{ CL_DEVICE_LOCAL_MEM_TYPE, BITFIELD, "LOCAL_MEM_TYPE" },
	{ CL_DEVICE_LOCAL_MEM_SIZE, ULONG, "LOCAL_MEM_SIZE" },
	{ CL_DEVICE_ERROR_CORRECTION_SUPPORT, BOOL, "ERROR_CORRECTION_SUPPORT" },
	{ CL_DEVICE_PROFILING_TIMER_RESOLUTION, SIZE, "PROFILING_TIMER_RESOLUTION" },
	{ CL_DEVICE_ENDIAN_LITTLE, BOOL, "ENDIAN_LITTLE" },
	{ CL_DEVICE_AVAILABLE, BOOL, "AVAILABLE" },
	{ CL_DEVICE_COMPILER_AVAILABLE, BOOL, "COMPILER_AVAILABLE" },
	{ CL_DEVICE_EXECUTION_CAPABILITIES, BITFIELD, "EXECUTION_CAPABILITIES" },
	{ CL_DEVICE_QUEUE_PROPERTIES, BITFIELD, "QUEUE_PROPERTIES" },
	{ CL_DEVICE_PLATFORM, BITFIELD, "PLATFORM" },
	{ CL_DEVICE_NAME, STRING, "NAME" },
	{ CL_DEVICE_VENDOR, STRING, "VENDOR" },
	{ CL_DRIVER_VERSION, STRING, "DRIVER_VERSION" },
	{ CL_DEVICE_PROFILE, STRING, "PROFILE" },
	{ CL_DEVICE_VERSION, STRING, "VERSION" },
	{ CL_DEVICE_EXTENSIONS, STRING, "EXTENSIONS" },
	{ 0, 0, NULL }
};

void
usage(void)
{
	fprintf(stderr, "usage: clGetDeviceIDs [device_id]\n");
	exit(1);
}

int
main(int argc, char *argv[])
{
	int retval = 0;

	cl_int status;

	if (argc != 2)
		usage();

	cl_device_id device = (cl_device_id)strtol(argv[1], NULL, 16);

	void *param_value;
	size_t param_value_size;
	struct device_info_t *device_info;
	for (device_info = device_info_list; device_info->name != NULL; device_info++)
	{
		status = clGetDeviceInfo(device, device_info->value, 0, NULL, &param_value_size);
		if (status != CL_SUCCESS)
		{
			printf("clGetDeviceInfo failed, device_info = %s, status = %d\n", device_info->name, status);
			continue;
		}
		else
		{
			param_value = malloc(param_value_size);
			assert(param_value != NULL);

			status = clGetDeviceInfo(device, device_info->value, param_value_size, param_value, NULL);
			if (status != CL_SUCCESS)
			{
				printf("clGetDeviceInfo failed, device_info = %s, status = %d\n", device_info->name, status);
				free(param_value);
				continue;
			}

			printf("%30s = ", device_info->name);
			switch (device_info->type)
			{
			case STRING:
				printf("\"%s\"\n", (char *)param_value);
				break;
			case UINT:
				printf("%u\n", *(cl_uint *)param_value);
				break;
			case ULONG:
				printf("%lu\n", *(cl_ulong *)param_value);
				break;
			case SIZE:
				printf("%lu\n", *(size_t *)param_value);
				break;
			case BOOL:
				printf("%s\n", *(cl_bool *)param_value ? "true" : "false");
				break;
			case BITFIELD:
				printf("0x%x\n", *(cl_uint *)param_value);
				break;
			}
			free(param_value);
		}
	}

	return retval;
}


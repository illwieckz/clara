/*-
 * Copyright 2009, 2010 Bjoern Koenig
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>

void
usage(void)
{
	fprintf(stderr, "usage: clGetDeviceIDs [platform_id]\n");
	exit(1);
}

int
main(int argc, char *argv[])
{
	int retval = 0;

	cl_int status;

	if (argc != 2)
		usage();

	cl_platform_id platform = (cl_platform_id)strtol(argv[1], NULL, 16);

	cl_uint num_entries;
	cl_device_id *devices;

	/* get number of available devices */
	status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, NULL, &num_entries);
	if (status != CL_SUCCESS)
	{
		fprintf(stderr, "clGetDeviceIDs failed, status = %d\n", status);
		retval = 1;
		goto exit_0;
	}

	/* allocate memory for device IDs */
	devices = malloc(num_entries);
	assert(devices != NULL);

	status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, num_entries, devices, NULL);
	if (status != CL_SUCCESS)
	{
		fprintf(stderr, "clGetDeviceIDs failed, status = %d\n", status);
		retval = 1;
		goto exit_1;
	}

	int i;
	for (i = 0; i < num_entries; i++)
		printf("%lx\n", (unsigned long)devices[i]);

exit_1:
	free(devices);
exit_0:
	return retval;
}


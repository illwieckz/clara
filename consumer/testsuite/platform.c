#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <CL/cl.h>

int
main()
{
	cl_uint num_platforms;
	cl_platform_id *platforms;

	if (clGetPlatformIDs(0, NULL, &num_platforms) != CL_SUCCESS)
	{
		printf("clGetPlatformIDs failed\n");
		return 1;
	}

	printf("num_platforms = %u\n", num_platforms);

	if (num_platforms == 0)
		return 0;

	platforms = calloc(num_platforms, sizeof(cl_platform_id));
	assert(platforms);

	if (clGetPlatformIDs(num_platforms, platforms, NULL) != CL_SUCCESS)
	{
		printf("clGetPlatformIDs failed\n");
		return 1;
	}

	int i;
	for (i = 0; i < num_platforms; i++)	
		printf("platform[%d] = %lx\n", i, (unsigned long)platforms[i]);

	usleep(10 * 1000 * 1000);

	return 0;
}
	
